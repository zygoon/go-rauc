# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Zygmunt Krynicki

VERSION 0.7
FROM golang:latest

WORKDIR /src

deps:
    COPY go.mod go.sum ./
    RUN go mod download
    SAVE ARTIFACT go.mod AS LOCAL go.mod
    SAVE ARTIFACT go.sum AS LOCAL go.sum

reuse:
    FROM fsfe/reuse:latest
    COPY . .
    RUN reuse lint

build:
    FROM +deps
    ENV GOCACHE=/go-cache
    COPY . .
    RUN --mount=type=cache,target=/go-cache go build ./...
    RUN --mount=type=cache,target=/go-cache go test ./...
