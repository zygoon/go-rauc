// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package installhandler contains logic needed by RAUC install handlers.
//
// RAUC install handlers are helper programs which can execute before and after
// each install operation. RAUC provides information about the operation with
// the help of environment variables.
//
// RAUC documentation [1] describes the protocol in detail
// [1] https://rauc.readthedocs.io/en/latest/reference.html
package installhandler

import (
	"fmt"
	"sort"
	"strings"
)

// Environment contains information provided by RAUC to an install handler.
type Environment struct {
	// MountPrefix corresponds to RAUC_MOUNT_PREFIX.
	MountPrefix string
	// BundleMountPoint corresponds to RAUC_BUNDLE_MOUNT_POINT.
	BundleMountPoint string
	// CurrentBootName corresponds to RAUC_CURRENT_BOOTNAME.
	CurrentBootName string
	// UpdateSource corresponds to RAUC_UPDATE_SOURCE.
	UpdateSource string
	// SystemConfig corresponds to RAUC_SYSTEM_CONFIG.
	SystemConfig string
	// TargetSlotIDs corresponds to RAUC_TARGET_SLOTS.
	TargetSlotIDs []string
	// SlotIDs corresponds to RAUC_SLOTS.
	SlotIDs []string
	// Slots correspond to data collected from a number of variables. See the
	// documentation of the Slot type for details.
	Slots map[string]*Slot
	// ImageIDs is synthesized and is not provided by RAUC explicitly.
	ImageIDs []string
	// Images correspond to data collected from a number of variables. See the
	// documentation of the Image type for details.
	Images map[string]*Image

	// InitialHandlerPID is the PID of initial handler process. The handler may be in a
	// special mount namespace, distinct from the rest of the system.
	InitialHandlerPID int

	// RemainingVariables are all the variables with the "RAUC_" prefix not parsed into
	// individual fields above. Over time the set of variables that are parsed may change,
	// and such variables may be transferred to dedicated, new fields of the outer type.
	// The only exception to this rule, and the only valid use of this field, is to access
	// custom variables between RAUC extensions.
	RemainingVariables map[string]string
}

// Slot contains information about RAUC slots provided to install handlers.
type Slot struct {
	// ID is the ephemeral ID assigned to this slot by RAUC, for the purpose of
	// conveying information to the install handler. Slot IDs should not be assumed
	// to be stable across invocation of the handler.
	ID string
	// Name corresponds to RAUC_SLOT_NAME_$id, where $id is Slot.ID.
	Name string
	// Class corresponds to RAUC_SLOT_CLASS_$id, where $id is Slot.ID.
	Class string
	// Type corresponds to RAUC_SLOT_TYPE_$id, where $id is Slot.ID.
	Type string
	// Type corresponds to RAUC_SLOT_BOOTNAME_$id, where $id is Slot.ID.
	BootName string
	// DevicePath corresponds to RAUC_SLOT_DEVICE_$id, where $id is Slot.ID.
	DevicePath string
	// ParentName corresponds to RAUC_SLOT_PARENT_$id, where $id is Slot.ID.
	ParentName string
}

// Image contains information about RAUC image provided to install handlers.
type Image struct {
	// ID is the ephemeral ID assigned to this image by RAUC, for the purpose of
	// conveying information to the install handler. Image IDs should not be assumed
	// to be stable across invocation of the handler.
	ID string
	// Name corresponds to RAUC_IMAGE_NAME_$id, where $id is Image.ID.
	Name string
	// Class corresponds to RAUC_IMAGE_CLASS_$id, where $id is Image.ID.
	Class string
	// Class corresponds to RAUC_IMAGE_DIGEST_$id, where $id is Image.ID.
	Digest string
}

// UnknownEnvError reports unknown environment variables in RAUC namespace.
//
// Deprecated: This error can never occur, as unknown variables are now
// collected into Environment.RemainingVariables.
type UnknownEnvError struct {
	Key   string
	Value string
}

// Error implements the error interface.
func (e UnknownEnvError) Error() string {
	return fmt.Sprintf("unknown environment variable %s=%s", e.Key, e.Value)
}

// Is returns true if the other error is an identical UnknownEnvError
func (e UnknownEnvError) Is(target error) bool {
	if t, ok := target.(UnknownEnvError); ok {
		return e == t
	}

	return false
}

// SlotReferenceError reports references to undefined slots.
type SlotReferenceError struct {
	SlotID string
	// EnvVar is the environment variable containing the reference.
	EnvVar string
}

// Error implements the error interface.
func (e SlotReferenceError) Error() string {
	return fmt.Sprintf("variable %s refers to undefined slot %s", e.EnvVar, e.SlotID)
}

// Is returns true if the other error is an identical SlotReferenceError
func (e SlotReferenceError) Is(target error) bool {
	if t, ok := target.(SlotReferenceError); ok {
		return e == t
	}

	return false
}

// EnvironmentError reports problems with install handler environment.
type EnvironmentError struct {
	Err error
}

// Error implements the error interface.
func (e EnvironmentError) Error() string {
	return fmt.Sprintf("cannot create handler environment: %v", e.Err)
}

// Unwrap returns the error wrapped inside the environment error.
func (e EnvironmentError) Unwrap() error {
	return e.Err
}

// NewEnvironment returns a new Environment described by the given environment block.
func NewEnvironment(environ []string) (*Environment, error) {
	env := Environment{}

	if err := env.parseInstallEnv(environ); err != nil {
		return nil, &EnvironmentError{Err: err}
	}

	env.synthesizeImageIDs()

	if err := env.validateSlotReferences(); err != nil {
		return nil, &EnvironmentError{Err: err}
	}

	return &env, nil
}

// Environ returns an environment block representing handler environment.
//
// A handler environment can be converted to and from the raw representation.
func (env *Environment) Environ() []string {
	// Initial capacity tuned to avoid realloc. The capacity matches
	// the number of append calls done below. Seven for the fixed fields,
	// then six for each slot and three for each image.
	environ := make([]string, 0, 7+len(env.Slots)*6+len(env.Images)*3+len(env.RemainingVariables))

	const keyValue = "%s=%s"

	environ = append(environ, fmt.Sprintf(keyValue, raucMountPrefixEnvKey, env.MountPrefix))
	environ = append(environ, fmt.Sprintf(keyValue, raucBundleMountPointEnvKey, env.BundleMountPoint))
	environ = append(environ, fmt.Sprintf(keyValue, raucCurrentBootNameEnvKey, env.CurrentBootName))
	environ = append(environ, fmt.Sprintf(keyValue, raucUpdateSourceEnvKey, env.UpdateSource))
	environ = append(environ, fmt.Sprintf(keyValue, raucSystemConfigEnvKey, env.SystemConfig))
	// Note that some versions of RAUC add an extra trailing space here.
	// This behavior is not reproduced.
	environ = append(environ, fmt.Sprintf(keyValue, raucTargetSlotsEnvKey, strings.Join(env.TargetSlotIDs, " ")))
	environ = append(environ, fmt.Sprintf(keyValue, raucSlotsEnvKey, strings.Join(env.SlotIDs, " ")))

	const keyIDValue = "%s%s=%s"

	for id, slot := range env.Slots {
		environ = append(environ, fmt.Sprintf(keyIDValue, raucSlotBootNameEnvKeyPrefix, id, slot.BootName))
		environ = append(environ, fmt.Sprintf(keyIDValue, raucSlotTypeEnvKeyPrefix, id, slot.Type))
		environ = append(environ, fmt.Sprintf(keyIDValue, raucSlotClassEnvKeyPrefix, id, slot.Class))
		environ = append(environ, fmt.Sprintf(keyIDValue, raucSlotDeviceEnvKeyPrefix, id, slot.DevicePath))
		environ = append(environ, fmt.Sprintf(keyIDValue, raucSlotNameEnvKeyPrefix, id, slot.Name))
		environ = append(environ, fmt.Sprintf(keyIDValue, raucSlotParentEnvKeyPrefix, id, slot.ParentName))
	}

	for id, image := range env.Images {
		environ = append(environ, fmt.Sprintf(keyIDValue, raucImageNameEnvKeyPrefix, id, image.Name))
		environ = append(environ, fmt.Sprintf(keyIDValue, raucImageClassEnvKeyPrefix, id, image.Class))
		environ = append(environ, fmt.Sprintf(keyIDValue, raucImageDigestEnvKeyPrefix, id, image.Digest))
	}

	for key, value := range env.RemainingVariables {
		environ = append(environ, fmt.Sprintf(keyValue, key, value))
	}

	// Sort for nice determinism.
	sort.Strings(environ)

	return environ
}

// parseInstallEnv parses RAUC handler environment from environment block.
//
// The resulting environment is partial and needs to be passed to
// synthesizeImageIDs and validate before being used.
func (env *Environment) parseInstallEnv(environ []string) error {
	for _, envItem := range environ {
		idx := strings.IndexRune(envItem, '=')
		if idx == -1 {
			continue
		}

		envKey, envValue := envItem[:idx], envItem[idx+1:]
		if !strings.HasPrefix(envKey, "RAUC_") {
			continue
		}

		if known := env.parseEnvItem(envKey, envValue); !known {
			if env.RemainingVariables == nil {
				env.RemainingVariables = make(map[string]string)
			}

			env.RemainingVariables[envKey] = envValue
		}
	}

	return nil
}

const (
	// Variables describing the rest of the handler environment.

	raucMountPrefixEnvKey      = "RAUC_MOUNT_PREFIX"
	raucBundleMountPointEnvKey = "RAUC_BUNDLE_MOUNT_POINT"
	raucUpdateSourceEnvKey     = "RAUC_UPDATE_SOURCE"
	raucCurrentBootNameEnvKey  = "RAUC_CURRENT_BOOTNAME"
	raucSystemConfigEnvKey     = "RAUC_SYSTEM_CONFIG"
	raucTargetSlotsEnvKey      = "RAUC_TARGET_SLOTS"
	raucSlotsEnvKey            = "RAUC_SLOTS"

	// variables describing RAUC slots.

	raucSlotBootNameEnvKeyPrefix = "RAUC_SLOT_BOOTNAME_"
	raucSlotTypeEnvKeyPrefix     = "RAUC_SLOT_TYPE_"
	raucSlotClassEnvKeyPrefix    = "RAUC_SLOT_CLASS_"
	raucSlotDeviceEnvKeyPrefix   = "RAUC_SLOT_DEVICE_"
	raucSlotNameEnvKeyPrefix     = "RAUC_SLOT_NAME_"
	raucSlotParentEnvKeyPrefix   = "RAUC_SLOT_PARENT_"

	// variables describing RAUC images.

	raucImageNameEnvKeyPrefix   = "RAUC_IMAGE_NAME_"
	raucImageClassEnvKeyPrefix  = "RAUC_IMAGE_CLASS_"
	raucImageDigestEnvKeyPrefix = "RAUC_IMAGE_DIGEST_"
)

func (env *Environment) parseEnvItem(envKey, envValue string) (known bool) {
	switch envKey {
	case raucMountPrefixEnvKey:
		env.MountPrefix = envValue
	case raucBundleMountPointEnvKey:
		env.BundleMountPoint = envValue
	case raucUpdateSourceEnvKey:
		env.UpdateSource = envValue
	case raucCurrentBootNameEnvKey:
		env.CurrentBootName = envValue
	case raucSystemConfigEnvKey:
		env.SystemConfig = envValue
	case raucTargetSlotsEnvKey:
		// Trim trailing spaces that are possible around this value in some versions of RAUC.
		envValue = strings.TrimSpace(envValue)
		if envValue != "" {
			IDs := strings.Split(envValue, " ")
			env.TargetSlotIDs = IDs
		}
	case raucSlotsEnvKey:
		// Trim trailing spaces that are possible around this value in some versions of RAUC.
		envValue = strings.TrimSpace(envValue)
		if envValue != "" {
			IDs := strings.Split(envValue, " ")
			env.SlotIDs = IDs
		}
	default:
		return env.parseSlotImageEnvItem(envKey, envValue)
	}

	return true
}

func (env *Environment) parseSlotImageEnvItem(envKey, envValue string) (known bool) {
	switch {
	// Slot attributes.
	case strings.HasPrefix(envKey, raucSlotNameEnvKeyPrefix):
		slotID := envKey[len(raucSlotNameEnvKeyPrefix):]
		slot := env.ensureSlot(slotID)
		slot.Name = envValue
	case strings.HasPrefix(envKey, raucSlotClassEnvKeyPrefix):
		slotID := envKey[len(raucSlotClassEnvKeyPrefix):]
		slot := env.ensureSlot(slotID)
		slot.Class = envValue
	case strings.HasPrefix(envKey, raucSlotTypeEnvKeyPrefix):
		slotID := envKey[len(raucSlotTypeEnvKeyPrefix):]
		slot := env.ensureSlot(slotID)
		slot.Type = envValue
	case strings.HasPrefix(envKey, raucSlotBootNameEnvKeyPrefix):
		slotID := envKey[len(raucSlotBootNameEnvKeyPrefix):]
		slot := env.ensureSlot(slotID)
		slot.BootName = envValue
	case strings.HasPrefix(envKey, raucSlotDeviceEnvKeyPrefix):
		slotID := envKey[len(raucSlotDeviceEnvKeyPrefix):]
		slot := env.ensureSlot(slotID)
		slot.DevicePath = envValue
	case strings.HasPrefix(envKey, raucSlotParentEnvKeyPrefix):
		slotID := envKey[len(raucSlotParentEnvKeyPrefix):]
		slot := env.ensureSlot(slotID)
		slot.ParentName = envValue
		// Image attributes.
	case strings.HasPrefix(envKey, raucImageNameEnvKeyPrefix):
		imageID := envKey[len(raucImageNameEnvKeyPrefix):]
		image := env.ensureImage(imageID)
		image.Name = envValue
	case strings.HasPrefix(envKey, raucImageClassEnvKeyPrefix):
		imageID := envKey[len(raucImageClassEnvKeyPrefix):]
		image := env.ensureImage(imageID)
		image.Class = envValue
	case strings.HasPrefix(envKey, raucImageDigestEnvKeyPrefix):
		imageID := envKey[len(raucImageDigestEnvKeyPrefix):]
		image := env.ensureImage(imageID)
		image.Digest = envValue
	default:
		return false
	}

	return true
}

func (env *Environment) ensureSlot(slotID string) (slot *Slot) {
	var ok bool

	if env.Slots == nil {
		env.Slots = make(map[string]*Slot)
	}

	if slot, ok = env.Slots[slotID]; !ok {
		slot = &Slot{ID: slotID}
		env.Slots[slotID] = slot
	}

	return slot
}

func (env *Environment) ensureImage(imageID string) (image *Image) {
	var ok bool

	if env.Images == nil {
		env.Images = make(map[string]*Image)
	}

	if image, ok = env.Images[imageID]; !ok {
		image = &Image{ID: imageID}
		env.Images[imageID] = image
	}

	return image
}

func (env *Environment) synthesizeImageIDs() {
	// Synthesize ImageIDs, similar to SlotIDs.
	// RAUC does not provide those explicitly.
	for id := range env.Images {
		env.ImageIDs = append(env.ImageIDs, id)
	}

	sort.Strings(env.ImageIDs)
}

func (env *Environment) validateSlotReferences() error {
	for _, slotID := range env.SlotIDs {
		if _, ok := env.Slots[slotID]; !ok {
			return &SlotReferenceError{SlotID: slotID, EnvVar: raucSlotsEnvKey}
		}
	}

	for _, slotID := range env.TargetSlotIDs {
		if _, ok := env.Slots[slotID]; !ok {
			return &SlotReferenceError{SlotID: slotID, EnvVar: raucTargetSlotsEnvKey}
		}
	}

	slotNameToID := make(map[string]string, len(env.Slots))
	for _, slot := range env.Slots {
		slotNameToID[slot.Name] = slot.ID
	}

	for _, slot := range env.Slots {
		if slot.ParentName != "" {
			parentID := slotNameToID[slot.ParentName]
			if _, ok := env.Slots[parentID]; !ok {
				return &SlotReferenceError{
					SlotID: slot.ParentName,
					EnvVar: raucSlotParentEnvKeyPrefix + slot.ID,
				}
			}
		}
	}

	return nil
}

// ImageWithClass returns an image with the given class.
func (env *Environment) ImageWithClass(cls string) *Image {
	for _, imageID := range env.ImageIDs {
		if image := env.Images[imageID]; image.Class == cls {
			return image
		}
	}

	return nil
}

// TargetSlotWithClass returns a target slot with the given class.
func (env *Environment) TargetSlotWithClass(cls string) *Slot {
	for _, slotID := range env.TargetSlotIDs {
		if slot := env.Slots[slotID]; slot.Class == cls {
			return slot
		}
	}

	return nil
}
