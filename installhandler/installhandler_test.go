// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package installhandler_test

import (
	"errors"
	"reflect"
	"testing"

	"gitlab.com/zygoon/go-rauc/installhandler"
)

var sampleEnv = []string{
	"STUFF_NOT_RELATED_TO_RAUC=potato",
	"MALFORMED_ENTRY",
	// Data obtained from spread.suite/rauc-install-handlers/task.yaml.
	"RAUC_BUNDLE_MOUNT_POINT=/run/rauc/bundle",
	"RAUC_CURRENT_BOOTNAME=A",
	"RAUC_IMAGE_CLASS_1=system",
	"RAUC_IMAGE_DIGEST_1=24365ef88606409b16e0d9ad7b52c835e46afa9e7a98e05a1394ccdabdf8ec41",
	"RAUC_IMAGE_NAME_1=system.img",
	"RAUC_MOUNT_PREFIX=/run/rauc",
	"RAUC_SLOT_BOOTNAME_1=B",
	"RAUC_SLOT_BOOTNAME_2=A",
	"RAUC_SLOT_CLASS_1=system",
	"RAUC_SLOT_CLASS_2=system",
	"RAUC_SLOT_DEVICE_1=/tmp/rauc-fake-system/slot-b",
	"RAUC_SLOT_DEVICE_2=/tmp/rauc-fake-system/slot-a",
	"RAUC_SLOT_NAME_1=system.1",
	"RAUC_SLOT_NAME_2=system.0",
	"RAUC_SLOT_PARENT_1=",
	"RAUC_SLOT_PARENT_2=",
	"RAUC_SLOTS=1 2 ",
	"RAUC_SLOT_TYPE_1=raw",
	"RAUC_SLOT_TYPE_2=raw",
	"RAUC_SYSTEM_CONFIG=/etc/rauc/system.conf",
	"RAUC_TARGET_SLOTS=1 ",
	"RAUC_UPDATE_SOURCE=/run/rauc/bundle",
	// Custom value in the RAUC_ namespace.
	"RAUC_CUSTOM=value",
}

func TestNewEnvironment(t *testing.T) {
	env, err := installhandler.NewEnvironment(sampleEnv)
	if err != nil {
		t.Fatal(err)
	}

	expected := &installhandler.Environment{
		MountPrefix:      "/run/rauc",
		BundleMountPoint: "/run/rauc/bundle",
		CurrentBootName:  "A",
		UpdateSource:     "/run/rauc/bundle",
		SystemConfig:     "/etc/rauc/system.conf",
		TargetSlotIDs:    []string{"1"},
		SlotIDs:          []string{"1", "2"},
		Slots: map[string]*installhandler.Slot{
			"1": {
				ID:         "1",
				Name:       "system.1",
				Class:      "system",
				Type:       "raw",
				BootName:   "B",
				DevicePath: "/tmp/rauc-fake-system/slot-b",
			},
			"2": {
				ID:         "2",
				Name:       "system.0",
				Class:      "system",
				Type:       "raw",
				BootName:   "A",
				DevicePath: "/tmp/rauc-fake-system/slot-a",
			},
		},
		ImageIDs: []string{"1"},
		Images: map[string]*installhandler.Image{
			"1": {
				ID:     "1",
				Name:   "system.img",
				Class:  "system",
				Digest: "24365ef88606409b16e0d9ad7b52c835e46afa9e7a98e05a1394ccdabdf8ec41",
			},
		},
		RemainingVariables: map[string]string{
			"RAUC_CUSTOM": "value",
		},
	}

	if !reflect.DeepEqual(env, expected) {
		t.Fatalf("Unexpected environment: %#v", env)
	}
}

func TestEnvironment_EmptyBlock(t *testing.T) {
	env, err := installhandler.NewEnvironment(nil)
	if err != nil {
		t.Fatal(err)
	}

	if len(env.SlotIDs) != 0 {
		t.Fatalf("SlotIDs should be empty")
	}

	if len(env.TargetSlotIDs) != 0 {
		t.Fatalf("TargetSlotIDs should be empty")
	}

	if len(env.ImageIDs) != 0 {
		t.Fatalf("ImageIDs should be empty")
	}
}

func TestEnvironment_EnvironRoundTrip(t *testing.T) {
	env1, err := installhandler.NewEnvironment(sampleEnv)
	if err != nil {
		t.Fatal(err)
	}

	env2, err := installhandler.NewEnvironment(env1.Environ())
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(env1, env2) {
		t.Fatalf("Sample environment does not round-trip through Environ")
	}
}

func TestEmptyEnviron(t *testing.T) {
	env1, err := installhandler.NewEnvironment(nil)
	if err != nil {
		t.Fatal(err)
	}

	environ := env1.Environ()
	if !reflect.DeepEqual(environ, []string{
		"RAUC_BUNDLE_MOUNT_POINT=",
		"RAUC_CURRENT_BOOTNAME=",
		"RAUC_MOUNT_PREFIX=",
		"RAUC_SLOTS=",
		"RAUC_SYSTEM_CONFIG=",
		"RAUC_TARGET_SLOTS=",
		"RAUC_UPDATE_SOURCE=",
	}) {
		t.Fatalf("Unexpected empty environment: %v", environ)
	}

	env2, err := installhandler.NewEnvironment(environ)
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(env1, env2) {
		t.Fatalf("Empty environment does not round-trip through Environ")
	}
}

func TestNewEnvironment_Errors(t *testing.T) {
	// Empty environment is valid but produces entirely empty environment.
	env, err := installhandler.NewEnvironment(nil)
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(env, &installhandler.Environment{}) {
		t.Fatalf("Zero value and return value of NewEnvironment(nil) differ")
	}

	// All variables in the RAUC_ namespace are either recognized or recorded.
	env, err = installhandler.NewEnvironment([]string{"RAUC_POTATO=1"})
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(env.RemainingVariables, map[string]string{"RAUC_POTATO": "1"}) {
		t.Fatalf("Unexpected remaining variables: %v", env.RemainingVariables)
	}

	// All slots referenced in RAUC_SLOTS must be defined.
	_, err = installhandler.NewEnvironment([]string{"RAUC_SLOTS=1"})
	if !errors.Is(err, installhandler.SlotReferenceError{SlotID: "1", EnvVar: "RAUC_SLOTS"}) {
		t.Fatalf("Unexpected error: %v", err)
	}

	// All slots referenced in RAUC_TARGET_SLOTS must be defined.
	_, err = installhandler.NewEnvironment([]string{"RAUC_TARGET_SLOTS=1"})
	if !errors.Is(err, installhandler.SlotReferenceError{SlotID: "1", EnvVar: "RAUC_TARGET_SLOTS"}) {
		t.Fatalf("Unexpected error: %v", err)
	}

	// All slots referenced in RAUC_SLOT_PARENT_$id must be defined.
	_, err = installhandler.NewEnvironment([]string{"RAUC_SLOT_PARENT_1=2"})
	if !errors.Is(err, installhandler.SlotReferenceError{SlotID: "2", EnvVar: "RAUC_SLOT_PARENT_1"}) {
		t.Fatalf("Unexpected error: %v", err)
	}
}

func TestImageWithClass(t *testing.T) {
	env, err := installhandler.NewEnvironment([]string{
		"RAUC_IMAGE_CLASS_1=system",
		"RAUC_IMAGE_DIGEST_1=24365ef88606409b16e0d9ad7b52c835e46afa9e7a98e05a1394ccdabdf8ec41",
		"RAUC_IMAGE_NAME_1=system.img",
	})
	if err != nil {
		t.Fatal(err)
	}

	if img := env.ImageWithClass("system"); !reflect.DeepEqual(img, &installhandler.Image{
		ID:     "1",
		Name:   "system.img",
		Class:  "system",
		Digest: "24365ef88606409b16e0d9ad7b52c835e46afa9e7a98e05a1394ccdabdf8ec41",
	}) {
		t.Fatalf("Unexpected image: %v", img)
	}

	if img := env.ImageWithClass("potato"); img != nil {
		t.Fatalf("Unexpected image: %v", img)
	}
}

func TestTargetSlotWithClass(t *testing.T) {
	env, err := installhandler.NewEnvironment([]string{
		"RAUC_SLOT_BOOTNAME_1=B",
		"RAUC_SLOT_BOOTNAME_2=A",
		"RAUC_SLOT_CLASS_1=system",
		"RAUC_SLOT_CLASS_2=system",
		"RAUC_SLOT_DEVICE_1=/tmp/rauc-fake-system/slot-b",
		"RAUC_SLOT_DEVICE_2=/tmp/rauc-fake-system/slot-a",
		"RAUC_SLOT_NAME_1=system.1",
		"RAUC_SLOT_NAME_2=system.0",
		"RAUC_SLOT_PARENT_1=",
		"RAUC_SLOT_PARENT_2=",
		"RAUC_SLOTS=1 2 ",
		"RAUC_SLOT_TYPE_1=raw",
		"RAUC_SLOT_TYPE_2=raw",
		"RAUC_TARGET_SLOTS=1 ",
	})
	if err != nil {
		t.Fatal(err)
	}

	if slot := env.TargetSlotWithClass("system"); !reflect.DeepEqual(slot, &installhandler.Slot{
		ID:         "1",
		Name:       "system.1",
		Class:      "system",
		Type:       "raw",
		BootName:   "B",
		DevicePath: "/tmp/rauc-fake-system/slot-b",
	}) {
		t.Fatalf("Unexpected slot: %#v", slot)
	}

	if slot := env.TargetSlotWithClass("potato"); slot != nil {
		t.Fatalf("Unexpected slot: %#v", slot)
	}

	// We are really looking at RAUC_TARGET_SLOTS.
	env, err = installhandler.NewEnvironment([]string{
		"RAUC_SLOT_BOOTNAME_1=B",
		"RAUC_SLOT_BOOTNAME_2=A",
		"RAUC_SLOT_CLASS_1=system",
		"RAUC_SLOT_CLASS_2=system",
		"RAUC_SLOT_DEVICE_1=/tmp/rauc-fake-system/slot-b",
		"RAUC_SLOT_DEVICE_2=/tmp/rauc-fake-system/slot-a",
		"RAUC_SLOT_NAME_1=system.1",
		"RAUC_SLOT_NAME_2=system.0",
		"RAUC_SLOT_PARENT_1=",
		"RAUC_SLOT_PARENT_2=",
		"RAUC_SLOTS=1 2 ",
		"RAUC_SLOT_TYPE_1=raw",
		"RAUC_SLOT_TYPE_2=raw",
		// NOTE: this is now empty.
		"RAUC_TARGET_SLOTS=",
	})
	if err != nil {
		t.Fatal(err)
	}

	if slot := env.TargetSlotWithClass("system"); slot != nil {
		t.Fatalf("Unexpected slot: %#v", slot)
	}
}

func TestRegressionGitLabIssue1(t *testing.T) {
	// We are really looking at RAUC_TARGET_SLOTS.
	env, err := installhandler.NewEnvironment([]string{
		"RAUC_BUNDLE_MOUNT_POINT=/run/rauc/bundle",
		"RAUC_CURRENT_BOOTNAME=b",
		"RAUC_IMAGE_CLASS_3=bootloader",
		"RAUC_IMAGE_CLASS_4=rootfs",
		"RAUC_IMAGE_DIGEST_3=(redacted)",
		"RAUC_IMAGE_DIGEST_4=(redacted)",
		"RAUC_IMAGE_NAME_3=(redacted)",
		"RAUC_IMAGE_NAME_4=(redacted)",
		"RAUC_MOUNT_PREFIX=/run/rauc",
		"RAUC_SLOT_BOOTNAME_1=",
		"RAUC_SLOT_BOOTNAME_2=b",
		"RAUC_SLOT_BOOTNAME_3=",
		"RAUC_SLOT_BOOTNAME_4=a",
		"RAUC_SLOT_CLASS_1=bootloader",
		"RAUC_SLOT_CLASS_2=rootfs",
		"RAUC_SLOT_CLASS_3=bootloader",
		"RAUC_SLOT_CLASS_4=rootfs",
		"RAUC_SLOT_DEVICE_1=/dev/mmcblk2boot1",
		"RAUC_SLOT_DEVICE_2=/dev/mmcblk2p3",
		"RAUC_SLOT_DEVICE_3=/dev/mmcblk2boot0",
		"RAUC_SLOT_DEVICE_4=/dev/mmcblk2p2",
		"RAUC_SLOT_NAME_1=bootloader.1",
		"RAUC_SLOT_NAME_2=rootfs.1",
		"RAUC_SLOT_NAME_3=bootloader.0",
		"RAUC_SLOT_NAME_4=rootfs.0",
		"RAUC_SLOT_PARENT_1=rootfs.1",
		"RAUC_SLOT_PARENT_2=",
		"RAUC_SLOT_PARENT_3=rootfs.0",
		"RAUC_SLOT_PARENT_4=",
		"RAUC_SLOT_TYPE_1=raw",
		"RAUC_SLOT_TYPE_2=raw",
		"RAUC_SLOT_TYPE_3=raw",
		"RAUC_SLOT_TYPE_4=raw",
		"RAUC_SLOTS=1 2 3 4",
		"RAUC_SYSTEM_CONFIG=/etc/rauc/system.conf",
		"RAUC_TARGET_SLOTS=3 4",
		"RAUC_UPDATE_SOURCE=/run/rauc/bundle",
	})
	if err != nil {
		t.Fatal(err)
	}

	if slot := env.Slots["1"]; slot.ParentName != "rootfs.1" {
		t.Fatalf("Unexpected parent of slot #1: %#v", slot)
	}

	if slot := env.Slots["3"]; slot.ParentName != "rootfs.0" {
		t.Fatalf("Unexpected parent of slot #3: %#v", slot)
	}
}
