// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package installhandler_test

import (
	"context"
	"errors"
	"testing"

	"gitlab.com/zygoon/go-cmdr/cmdtest"

	"gitlab.com/zygoon/go-rauc/installhandler"
)

func TestHandlerCmd(t *testing.T) {
	// See the description of HandlerFunc for details.
	const phantomArg = ""

	t.Run("help", func(t *testing.T) {
		inv := cmdtest.Invoke(&installhandler.Cmd{}, "--help")

		if err := inv.ExpectExitCode(64); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStdout(""); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStderr("Usage of installhandler:\n"); err != nil {
			t.Log(err)
			t.Fail()
		}
	})

	t.Run("no-function", func(t *testing.T) {
		inv := cmdtest.Invoke(&installhandler.Cmd{}, phantomArg)

		if err := inv.ExpectExitCode(0); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStdout(""); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStderr(""); err != nil {
			t.Log(err)
			t.Fail()
		}
	})

	t.Run("unhappy-function", func(t *testing.T) {
		inv := cmdtest.Invoke(&installhandler.Cmd{
			Func: func(context.Context, *installhandler.Environment, *installhandler.Messenger, string) error {
				return errors.New("boom")
			},
		}, phantomArg)

		if err := inv.ExpectExitCode(1); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStdout("<< error boom\n"); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStderr(""); err != nil {
			t.Log(err)
			t.Fail()
		}
	})

	t.Run("happy-function", func(t *testing.T) {
		inv := cmdtest.Invoke(&installhandler.Cmd{
			Func: func(context.Context, *installhandler.Environment, *installhandler.Messenger, string) error {
				return nil
			},
		}, phantomArg)

		if err := inv.ExpectExitCode(0); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStdout(""); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStderr(""); err != nil {
			t.Log(err)
			t.Fail()
		}
	})

	t.Run("all-msgs", func(t *testing.T) {
		inv := cmdtest.Invoke(&installhandler.Cmd{
			Func: func(_ context.Context, _ *installhandler.Environment, msg *installhandler.Messenger, arg string) error {
				_ = msg.EmitDebugMsg("this is a debug message")
				_ = msg.EmitErrorMsg("this is an error message")
				_ = msg.SetHandlerStatus("status-name")
				_ = msg.SetBootLoaderStatus("status-name")
				_ = msg.SetImageStatus("image-name", "status-name")

				return nil
			},
		}, phantomArg)

		if err := inv.ExpectExitCode(0); err != nil {
			t.Log(err)
			t.Fail()
		}

		const expected = `<< debug this is a debug message
<< error this is an error message
<< handler status-name
<< bootloader status-name
<< image image-name status-name
`

		if err := inv.ExpectStdout(expected); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStderr(""); err != nil {
			t.Log(err)
			t.Fail()
		}
	})

	t.Run("argument", func(t *testing.T) {
		inv := cmdtest.Invoke(&installhandler.Cmd{
			Func: func(_ context.Context, _ *installhandler.Environment, msg *installhandler.Messenger, arg string) error {
				if arg != "potato" {
					t.Fatalf("Unexpected argument: %v", arg)
				}

				return nil
			},
		}, "potato")

		if err := inv.ExpectExitCode(0); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStdout(""); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStderr(""); err != nil {
			t.Log(err)
			t.Fail()
		}
	})

	t.Run("no-arguments", func(t *testing.T) {
		inv := cmdtest.Invoke(&installhandler.Cmd{
			Func: func(_ context.Context, _ *installhandler.Environment, msg *installhandler.Messenger, arg string) error {
				// This is emulating RAUC 1.9 or older.
				if arg != "" {
					t.Fatalf("Unexpected argument: %v", arg)
				}

				return nil
			},
		})

		if err := inv.ExpectExitCode(0); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStdout(""); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStderr(""); err != nil {
			t.Log(err)
			t.Fail()
		}
	})

	t.Run("arguments-combined", func(t *testing.T) {
		inv := cmdtest.Invoke(&installhandler.Cmd{
			Func: func(_ context.Context, _ *installhandler.Environment, msg *installhandler.Messenger, arg string) error {
				if arg != "potato ham cheese" {
					t.Fatalf("Unexpected argument: %v", arg)
				}

				return nil
			},
		}, "potato", "ham", "cheese")

		if err := inv.ExpectExitCode(0); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStdout(""); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStderr(""); err != nil {
			t.Log(err)
			t.Fail()
		}
	})

	t.Run("arguments-separate", func(t *testing.T) {
		inv := cmdtest.Invoke(&installhandler.Cmd{
			FuncV: func(_ context.Context, _ *installhandler.Environment, msg *installhandler.Messenger, args []string) error {
				if len(args) != 3 || args[0] != "potato" || args[1] != "ham" || args[2] != "cheese" {
					t.Fatalf("Unexpected arguments: %v", args)
				}

				return nil
			},
		}, "potato", "ham", "cheese")

		if err := inv.ExpectExitCode(0); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStdout(""); err != nil {
			t.Log(err)
			t.Fail()
		}

		if err := inv.ExpectStderr(""); err != nil {
			t.Log(err)
			t.Fail()
		}
	})
}
