// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package installhandler

import (
	"context"
	"errors"
	"flag"
	"os"
	"strings"

	"gitlab.com/zygoon/go-cmdr"
)

// Cmd may be used as a RAUC pre or post install handler command.
//
// Cmd type should be used with gitlab.com/zygoon/go-cmdr module.
type Cmd struct {
	// If Func is non-nil and RAUC 1.10 or newer is used, then all the arguments
	// are combined with a single space. This is the default behavior of RAUC
	// 1.9 or older.
	//
	// Deprecated: use FuncV instead.
	Func         HandlerFunc  // For RAUC v1.9 or older.
	FuncV        HandlerFuncV // For RAUC v1.10 or newer.
	OneLinerText string
}

// HandlerFunc is the function invoked by the install handler command.
//
// The function is called with a context, RAUC install handler environment,
// RAUC messenger and the sole argument. RAUC, at least since version 1.8,
// passes one argument to the handler programs. It may either be the empty
// string, or the argument provided by the bundle manifest, or the argument
// provided by the bundle manifest and the RAUC startup argument
// --handler-args=.
//
// This is compatible with up to RAUC v1.9. For RAUC v1.10 or newer use
// HandlerFuncV instead.
type HandlerFunc func(context.Context, *Environment, *Messenger, string) error

// HandlerFuncV is like HandlerFunc but accepts multiple arguments.
//
// RAUC v1.10 changes the way arguments are passed to the handler programs. It
// now passes all arguments provided by the bundle manifest and the RAUC
// startup argument --handler-args=.
type HandlerFuncV func(context.Context, *Environment, *Messenger, []string) error

// OneLiner returns Cmd.OneLinerText
//
// OneLiner implements router.OneLiner interface, which aids in constructing
// sub-command trees with router.Cmd type.
func (cmd *Cmd) OneLiner() string {
	return cmd.OneLinerText
}

// Run acts as a RAUC {pre,post}-install handler function.
//
// Run parses the handler environment and calls Cmd.Func, if not nil.
//
// Run implements cmdr.Cmd interface and may be used with cmdr.RunMain or
// router.Cmd to construct multiplexing binaries.
func (cmd *Cmd) Run(ctx context.Context, args []string) (err error) {
	_, stdout, stderr := cmdr.Stdio(ctx)
	msg := NewMessenger(stdout)

	defer func() {
		if err != nil && !errors.Is(err, flag.ErrHelp) {
			// Convey the error to RAUC using the messenger protocol. The error
			// message produced by cmdr.RunMain is ignored by RAUC.
			_ = msg.EmitErrorMsg(err.Error())
			err = cmdr.SilentError(1)
		}
	}()

	fl := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
	fl.SetOutput(stderr)

	if err := fl.Parse(args); err != nil {
		return err
	}

	env, err := NewEnvironment(os.Environ())
	if err != nil {
		return err
	}

	switch {
	case cmd.FuncV != nil:
		return cmd.FuncV(ctx, env, msg, fl.Args())
	case cmd.Func != nil:
		combinedArgs := strings.Join(fl.Args(), " ")
		return cmd.Func(ctx, env, msg, combinedArgs)
	}

	return nil
}
