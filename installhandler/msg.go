// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package installhandler

import (
	"errors"
	"fmt"
	"io"
	"strings"
)

// ErrSpacesNotAllowed records attempt to use spaces in image name or status value.
var ErrSpacesNotAllowed = errors.New("spaces are not allowed")

// Messenger allows sending one way messages to RAUC.
//
// Messages produced with the messenger are not visible in plain output of "rauc
// install", at least not as of version 1.8, but they do show up in log output.
type Messenger struct {
	w io.Writer
}

// NewMessenger returns a messenger using the given writer.
//
// Ordinary messenger needs to provide formatted output to stdout.
func NewMessenger(w io.Writer) *Messenger {
	return &Messenger{w: w}
}

// EmitDebugMsg sends a debug message to RAUC.
//
// RAUC prints the message back prefixed with "Debug: ".
func (m Messenger) EmitDebugMsg(msg string) (err error) {
	_, err = fmt.Fprintln(m.w, "<< debug", msg)

	return err
}

// EmitErrorMsg sends an error message to RAUC.
//
// RAUC prints the message back prefixed with "Error: ".
func (m Messenger) EmitErrorMsg(msg string) (err error) {
	_, err = fmt.Fprintln(m.w, "<< error", msg)

	return err
}

// SetHandlerStatus sends handler status to RAUC.
//
// RAUC prints the status prefixed with "Handler status: ". Returns
// ErrSpacesNotAllowed if the status string contains spaces.
func (m Messenger) SetHandlerStatus(st string) (err error) {
	if strings.ContainsRune(st, ' ') {
		return ErrSpacesNotAllowed
	}

	_, err = fmt.Fprintln(m.w, "<< handler", st)

	return err
}

// SetImageStatus sends status of a given image to RAUC.
//
// RAUC prints the status prefixed with "Image '%s' status: ". Returns
// ErrSpacesNotAllowed if the image name or status string contain spaces.
func (m Messenger) SetImageStatus(im, st string) (err error) {
	if strings.ContainsRune(im, ' ') || strings.ContainsRune(st, ' ') {
		return ErrSpacesNotAllowed
	}

	_, err = fmt.Fprintln(m.w, "<< image", im, st)

	return err
}

// SetBootLoaderStatus sends boot loader status to RAUC.
//
// RAUC prints the status prefixed with "Bootloader status: ". Returns
// ErrSpacesNotAllowed if the status string contains spaces.
func (m Messenger) SetBootLoaderStatus(st string) (err error) {
	if strings.ContainsRune(st, ' ') {
		return ErrSpacesNotAllowed
	}

	_, err = fmt.Fprintln(m.w, "<< bootloader", st)

	return err
}
