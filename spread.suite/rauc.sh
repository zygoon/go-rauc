#!/bin/sh
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.
set -e
set -u
set -x

case "${1:-}" in
    prepare)
        # Configure RAUC. Since RAUC is a critical dependency having this
        # configuration done globally makes test suite preparations easier.
        mkdir -p "$TEST_RAUC_KEYS_DIR"
        mkdir -p "$TEST_RAUC_FAKE_SYSTEM_DIR"
        mkdir -p /etc/rauc

        # Create a certificate to get RAUC to sign the bundle.
        openssl req -x509 -newkey rsa:4096 \
            -keyout "$TEST_RAUC_KEYS_DIR/key.pem" \
            -out "$TEST_RAUC_KEYS_DIR/cert.pem" \
            -days 365 -nodes -subj "/C=PL/ST=Fake/L=Fake" </dev/null
        test -f "$TEST_RAUC_KEYS_DIR/key.pem"
        test -f "$TEST_RAUC_KEYS_DIR/cert.pem"

        # Create a fake rauc system configuration file containing the A/B
        # slots. Note that the name ends with .vanilla and we use it as a
        # template for prepare-each.
        cat <<RAUC_SYSTEM_CONF >/etc/rauc/system.conf.vanilla
[system]
compatible=Test Environment

# Use a custom bootloader but do NOT set the backend program as that
# is specified in each test.
bootloader=custom
statusfile=$TEST_RAUC_FAKE_SYSTEM_DIR/status.raucs
bundle-formats=plain

[slot.system.0]
device=$TEST_RAUC_FAKE_SYSTEM_DIR/slot-a
bootname=A

[slot.system.1]
device=$TEST_RAUC_FAKE_SYSTEM_DIR/slot-b
bootname=B

[keyring]
path=$TEST_RAUC_KEYS_DIR/cert.pem
RAUC_SYSTEM_CONF

        touch "$TEST_RAUC_FAKE_SYSTEM_DIR"/slot-a
        touch "$TEST_RAUC_FAKE_SYSTEM_DIR"/slot-b
        ;;

    prepare-each)
        # Stop RAUC to allow task-level prepare to modify the configuration file.
        systemctl stop rauc.service || true

        # Truncate the slot files to avoid leaking state across tests.
        truncate --size=0 "$TEST_RAUC_FAKE_SYSTEM_DIR/slot-a"
        truncate --size=0 "$TEST_RAUC_FAKE_SYSTEM_DIR/slot-b"

        # Remove RAUC state file to make sure we avoid leaking across tests.
        rm -f "$TEST_RAUC_FAKE_SYSTEM_DIR/status.raucs"

        # Copy the vanilla rauc system configuration file to give each test
        # a chance to modify it.
        cp /etc/rauc/system.conf.vanilla /etc/rauc/system.conf
        ;;
    restore)
        # Stop RAUC again since we want to remove all configuration.
        systemctl stop rauc.service || true

        rm -rf /etc/rauc
        rm -rf "$TEST_RAUC_KEYS_DIR"
        rm -rf "$TEST_RAUC_FAKE_SYSTEM_DIR"
        ;;
    debug)
        test -f "$TEST_RAUC_FAKE_SYSTEM_DIR/status.raucs" && cat "$TEST_RAUC_FAKE_SYSTEM_DIR/status.raucs"
        ;;
esac
