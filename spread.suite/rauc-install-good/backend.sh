#!/bin/sh -e
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

# Log the request to a fixed file as well as to stderr (for debugging)
echo "custom: $*" >&2

# Using /tmp/backend.state as the state variable, implement a state machine
# which handles the expected request and gives a canned response.
#
# Everything written to stdout is a part of the custom boot backend:
# https://rauc.readthedocs.io/en/latest/integration.html#custom-bootloader-backend-interface
state="$(cat /tmp/backend.state)"
case "${state}" in
    # This is what happens after: rauc status
    0-0)
        test "$*" = "get-primary" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo "A"
        echo "custom: $* -> A" >> /tmp/backend.log
        echo 0-1 >/tmp/backend.state
        ;;
    0-1)
        test "$*" = "get-state B" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo "bad"
        echo "custom: $* -> bad" >> /tmp/backend.log
        echo 0-2 >/tmp/backend.state
        ;;
    0-2)
        test "$*" = "get-state A" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo "good"
        echo "custom: $* -> good" >> /tmp/backend.log
        echo 1-0 >/tmp/backend.state
        ;;
    # This is what happens after: rauc install bundle.img
    1-0)
        test "$*" = "set-state B bad" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo 1-1 >/tmp/backend.state
        echo "custom: $*" >> /tmp/backend.log
        ;;
    1-1)
        test "$*" = "set-primary B" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo "custom: $*" >> /tmp/backend.log
        echo 1-2 >/tmp/backend.state
        ;;
    # This is what happens after: rauc status mark-good
    1-2)
        test "$*" = "get-primary" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo "B"
        echo "custom: $* -> B" >> /tmp/backend.log
        echo 1-3 >/tmp/backend.state
        ;;
    1-3)
        test "$*" = "get-state B" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo "bad"
        echo "custom: $* -> bad" >> /tmp/backend.log
        echo 1-4 >/tmp/backend.state
        ;;
    1-4)
        test "$*" = "get-state A" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo "good"
        echo "custom: $* -> good" >> /tmp/backend.log
        echo 1-5 >/tmp/backend.state
        ;;
    1-5)
        test "$*" = "set-state B good" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo "custom: $*" >> /tmp/backend.log
        echo 2-0 >/tmp/backend.state
        ;;
    # This is what happens after: rauc status (2nd call)
    2-0)
        test "$*" = "get-primary" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo "B"
        echo "custom: $* -> B" >> /tmp/backend.log
        echo 2-1 >/tmp/backend.state
        ;;
    2-1)
        test "$*" = "get-state B" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo "good"
        echo "custom: $* -> good" >> /tmp/backend.log
        echo 2-2 >/tmp/backend.state
        ;;
    2-2)
        test "$*" = "get-state A" || (echo "unexpected request in state $state: $*" >&2; exit 1)
        echo "bad"
        echo "custom: $* -> bad" >> /tmp/backend.log
        echo 3-0 >/tmp/backend.state
        ;;
    *)
        echo "unexpected state: $state" >&2
        echo "custom: $* -> ???" >> /tmp/backend.log
        exit 1
        ;;
esac
