# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

summary: play-through of "rauc install" and "rauc status mark-good"
details: |
    Using a custom boot backend, record what "rauc install",
    followed by "rauc mark-good" is doing on the system.
prepare: |
    mkdir bundle-dir
    mkdir system-dir

    # Create a fake system image
    mkdir system-dir/etc
    printf "ID=test\nID_VERSION=1\nID_VARIANT=test-variant\n" > system-dir/etc/os-release

    mksquashfs system-dir bundle-dir/system.img -comp zstd

    # Create a fake rauc bundle with that system image
    cat <<RAUC_MANIFEST >bundle-dir/manifest.raucm
    [update]
    compatible=Test Environment
    version=1

    [image.system]
    filename=system.img
    RAUC_MANIFEST

    # Modify the fake rauc configuration file to set a custom bootloader handler.
    cat <<RAUC_SYSTEM_CONF >>/etc/rauc/system.conf
    [handlers]
    bootloader-custom-backend=$(pwd)/backend.sh
    RAUC_SYSTEM_CONF

    rauc bundle --cert="$TEST_RAUC_KEYS_DIR/cert.pem" --key="$TEST_RAUC_KEYS_DIR/key.pem" bundle-dir bundle.img

    # Store /proc/cmdline and allow us to fake /proc/cmdline easily in the execute phase below.
    cp /proc/cmdline cmdline.orig
    cp cmdline.orig cmdline
    mount --bind cmdline /proc/cmdline

    # Stop rauc and grab a cursor for the journal log
    systemctl stop rauc.service || true
    journalctl -u rauc.service --cursor-file=cursor >/dev/null || true

    # Truncate the backend log and reset backend state
    truncate --size=0 /var/backend.log
    echo 0-0 >/tmp/backend.state

execute: |
    printf "%s rauc.slot=A\n" "$(cat cmdline.orig)" > cmdline

    # Run rauc status to examine the state of the system before the install
    # process. Note that this partially returns fake information about the
    # system that we provide in backend.sh, so it is not entirely authoritative.
    # At the same time it shows if RAUC is _happy_ with our answers.
    rauc status --output-format=json 2>rauc-status.err | jq --sort-keys --indent 4 --monochrome-output > status-before.json
    if [ -s rauc-status.err ]; then
        echo "rauc status failed:"
        cat rauc-status.err
        exit 1
    fi

    # The backend was called as we expect.
    diff -u /tmp/backend.log expected-status-before.log
    # The status, as RAUC sees it, is what we expect.
    diff -u status-before.json expected-status-before.json
    # Truncate the log for another command.
    truncate --size=0 /tmp/backend.log

    # Install the bundle with the updated system image.
    rauc install bundle.img

    # The backend was called as we expect.
    diff -u /tmp/backend.log expected-install.log
    # Truncate the log for another command.
    truncate --size=0 /tmp/backend.log

    # The slot contains the image we prescribed.
    cmp "$TEST_RAUC_FAKE_SYSTEM_DIR/slot-b" bundle-dir/system.img

    # On a real system, we would reboot here.

    # Here we stop rauc.service to discard any state, rig /proc/cmdline
    # to pretend we've booted the B slot and use mark-good to indicate
    # that the update has worked fine.
    systemctl stop rauc.service
    printf "%s rauc.slot=B\n" "$(cat cmdline.orig)" > cmdline
    rauc status mark-good

    # The backend was called as we expected.
    diff -u /tmp/backend.log expected-status-mark-good.log
    # Truncate the log for another command.
    truncate --size=0 /tmp/backend.log

    # Run rauc status to examine the state of the system after the install
    # process. The same caveat as on the earlier "rauc status" call applies.
    rauc status --output-format=json 2>rauc-status.err | jq --sort-keys --indent 4 --monochrome-output > status-after.json
    if [ -s rauc-status.err ]; then
        echo "rauc status failed:"
        cat rauc-status.err
        exit 1
    fi

    # The backend was called as we expect.
    diff -u /tmp/backend.log expected-status-after.log
    # The status, as RAUC sees it, is what we expect.
    diff -u status-after.json expected-status-after.json

restore: |
    umount /proc/cmdline || true

    rm -rf system-dir bundle-dir
    rm -f *.img
    rm -f *.pem
    rm -f *.json
    rm -f /tmp/backend.{log,state}
    rm -f cursor
    rm -f cmdline{,.orig}

debug: |
    cat /proc/cmdline
    test -f /tmp/backend.log && cat /tmp/backend.log
    test -f /tmp/backend.state && cat /tmp/backend.state
    journalctl --cursor-file=cursor -u rauc.service
