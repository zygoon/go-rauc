# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

stages:
  - compliance
  - test
  - build
  - integration
  - deploy

include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - project: 'zygoon/go-gitlab-ci'
    ref: v0.4.0
    file: '/go.yml'

dco:
  interruptible: true
  stage: compliance
  image: christophebedard/dco-check:latest
  tags: [linux, docker, x86_64]
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_EXTERNAL_PULL_REQUEST_IID
    - if: $CI_COMMIT_BRANCH == '$CI_DEFAULT_BRANCH'
  script:
    # Work around a bug in dco-check affecting pipelines for merge requests.
    # https://github.com/christophebedard/dco-check/issues/104
    - |
        if [ "${CI_MERGE_REQUEST_EVENT_TYPE:-}" = detached ]; then
            git fetch -a  # so that we can resolve branch names below
            export CI_COMMIT_BRANCH="$CI_COMMIT_REF_NAME";
            export CI_COMMIT_BEFORE_SHA="$CI_MERGE_REQUEST_DIFF_BASE_SHA";
            export CI_MERGE_REQUEST_SOURCE_BRANCH_SHA="$(git rev-parse "origin/$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME")";
            export CI_MERGE_REQUEST_TARGET_BRANCH_SHA="$(git rev-parse "origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME")";
        fi
    - dco-check --default-branch-from-remote --verbose

reuse:
  interruptible: true
  stage: compliance
  tags: [linux, docker, x86_64]
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  script:
    - reuse lint

go-test:
  parallel:
    matrix:
      - CI_GO_VERSION: ["1.18", latest]

.go-build:
  variables:
    # This is already done by go-gitlab-ci but setting it explicitly makes it
    # easier to "git grep" for the value and see where it is used. In some
    # places using variables is hard but it is still important to have a
    # synchronized behavior.
    GOFLAGS: -ldflags=-s -ldflags=-w -trimpath
    # Disable cgo to avoid linking to host libc.
    CGO_ENABLED: "0"

.spread:
  interruptible: true
  tags: [linux, docker, x86_64, kvm]
  stage: integration
  image: zyga/spread:latest
  variables:
    DEBIAN_FRONTEND: noninteractive
    SPREAD_QEMU_KVM: "1"
    TRANSFER_METER_FREQUENCY: "2s"
    ARTIFACT_COMPRESSION_LEVEL: "fast"
    CACHE_COMPRESSION_LEVEL: "fastest"
  parallel:
    matrix:
      - VERSION_CODENAME: [jammy]
        ID: [ubuntu]
        ID_VERSION: ["22.04"]
  before_script:
    - mkdir -p .cache
    - mkdir -p ~/.spread/qemu
    - |
      set -ex
      if [ ! -e .cache/autopkgtest-$VERSION_CODENAME-amd64.img ]; then
        # Prepare a qemu image that is useful for testing. For now this is just
        # a vanilla Ubuntu image with some packages pre-installed to speed up
        # the project prepare step in spread.yaml.
        cd .cache
        apt-get update && apt-get install -y autopkgtest qemu-utils genisoimage ca-certificates python3-distro-info
        time autopkgtest-buildvm-ubuntu-cloud --verbose -r $VERSION_CODENAME --post-command "apt-get -y install golang-go jq git"
        cd ..
      fi
    - ln -s $CI_PROJECT_DIR/.cache/autopkgtest-$VERSION_CODENAME-amd64.img ~/.spread/qemu/$ID-$ID_VERSION-64.img
  script:
    - ':'
  cache:
    # If the image needs to be rebuilt, just bump this number.
    key: autopkgtest-image-$VERSION_CODENAME-3
    # The image compresses well. Don't compress it here, GitLab will compress
    # it automatically as a part of the cache creation step.
    paths:
     - .cache/autopkgtest-$VERSION_CODENAME-amd64.img

spread-cache:
  extends: .spread
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'

spread/linux/x86_64:
  extends: .spread
  needs:
    - job: spread-cache
      optional: true
  script:
    - 'time spread -v qemu:$ID-$ID_VERSION-64:'
  cache:
    policy: pull

earthly:
  image: earthly/earthly:v0.7.1
  services:
    - docker:dind
  tags: [linux, x86_64, docker, dind]
  stage: build
  variables:
    DOCKER_HOST: tcp://docker:2375
    FORCE_COLOR: 1
    EARTHLY_EXEC_CMD: "/bin/sh"
  before_script:
    - earthly bootstrap
  script:
    - earthly --ci +build

.cross-build:
  rules:
  - when: never
